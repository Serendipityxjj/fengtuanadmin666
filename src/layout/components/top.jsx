import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";
import React from "react";
// 导入组件
import { Breadcrumb } from "antd";
function Top(props) {
  let { collapsed, setCollapsed } = props;
  return (
    <>
      <div className="left">
        <div className="menuIcon">
          {React.createElement(
            collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
            {
              className: "trigger",
              onClick: () => setCollapsed(!collapsed),
            }
          )}
        </div>
        <div className="bread">
          <Breadcrumb>
            <Breadcrumb.Item>后台首页</Breadcrumb.Item>
            <Breadcrumb.Item>用户模块</Breadcrumb.Item>
            <Breadcrumb.Item>用户列表</Breadcrumb.Item>
          </Breadcrumb>
        </div>
      </div>
      <div className="right">神龙教主（超级管理员）</div>
    </>
  );
}
export default Top;
