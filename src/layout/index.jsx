// 导入样式
import { Container } from "./style";
// 导入API
import React, { useState } from "react";
import { useNavigate, Outlet } from "react-router-dom";
// 导入组件
import Top from "./components/top";
import { AppstoreOutlined, EnvironmentOutlined } from "@ant-design/icons";
import { Layout, Menu } from "antd";

const { Header, Sider, Content } = Layout;

const Layout1 = () => {
  // 状态
  const [collapsed, setCollapsed] = useState(false);
  // 路由
  const navigator = useNavigate();
  // 返回jsx
  return (
    <Container>
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo" />
          <Menu
            onClick={({ keyPath }) => {
              let urls = {
                "1-0": "/admin",
                "2-1": "/admin/users",
                "2-2": "/admin/users/create",
                "3-1": "/admin/cates",
                "3-2": "/admin/cates/create",
              };
              let key = keyPath.shift();
              // 自己想到判断的方法 需要总结
              navigator(urls[key]);
            }}
            theme="dark"
            mode="inline"
            defaultSelectedKeys={["1"]}
            items={[
              {
                key: "1-0",
                icon: <AppstoreOutlined />,
                label: "后台首页",
              },
              {
                key: "2-0",
                icon: <AppstoreOutlined />,
                label: "用户管理",
                children: [
                  {
                    key: "2-1",
                    icon: <EnvironmentOutlined />,
                    label: "用户列表",
                  },
                  {
                    key: "2-2",
                    icon: <EnvironmentOutlined />,
                    label: "用户创建",
                  },
                ],
              },
              {
                key: "3-0",
                icon: <AppstoreOutlined />,
                label: "分类管理",
                children: [
                  {
                    key: "3-1",
                    icon: <EnvironmentOutlined />,
                    label: "分类列表",
                  },
                  {
                    key: "3-2",
                    icon: <EnvironmentOutlined />,
                    label: "分类创建",
                  },
                ],
              },
            ]}
          />
        </Sider>
        <Layout
          className="site-layout"
          style={{
            overflow: "hidden",
          }}
        >
          <Header
            className="site-layout-background"
            style={{
              padding: 0,
            }}
          >
            <Top collapsed={collapsed} setCollapsed={setCollapsed} />
          </Header>
          <Content
            className="site-layout-background"
            style={{
              margin: "24px 16px",
              padding: 24,
              minHeight: 280,
              overflowY: "scroll",
            }}
          >
            <Outlet />
          </Content>
        </Layout>
      </Layout>
    </Container>
  );
};

export default Layout1;
