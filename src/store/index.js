import { createStore, applyMiddleware } from "redux";
import { combineReducers } from "redux-immutable";
import thunk from "redux-thunk";
import example from "../pages/example/store";
import users from "../pages/users/store";
export default createStore(
  combineReducers({
    example,
    users,
  }),
  applyMiddleware(thunk)
);
