// 导入样式
import { Container } from "./styled.js";
// 导入API
import { useNavigate } from "react-router-dom";
import { postLoginapi } from "../../api/login.js";
// 导入组件
import { Button, Form, Input, Select, message } from "antd";
const { Option } = Select;
function Login() {
  // 路由相关
  const navigate = useNavigate();
  const onFinish = async (values) => {
    // 表单登录相关
    console.log("success", values);
    let res = await postLoginapi(values);
    if (res.meta.state === 200) {
      localStorage.setItem("uname", res.data.uname);
      localStorage.setItem("token", res.data.token);
      localStorage.setItem("roleName", res.data.roleName);
      message.success("登陆成功");
      navigate("/admin");
    } else {
      message.error(res.meta.msg);
    }
  };
  const onFinishFailed = (errorInfo) => {
    console.log("success", errorInfo);
  };
  return (
    <Container>
      <Form
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item>
          <h1>锋团本地生活平台</h1>
        </Form.Item>
        <Form.Item
          name="question"
          rules={[{ required: true, message: "请选择密保问题" }]}
        >
          <Select placeholder="请选择密保问题">
            <Option value="你爷爷的名字">你爷爷的名字</Option>
            <Option value="你奶奶的名字">你奶奶的名字</Option>
            <Option value="您其中一位老师的名字">您其中一位老师的名字</Option>
          </Select>
        </Form.Item>
        <Form.Item
          name="answer"
          rules={[{ required: true, message: "请输入密保答案" }]}
        >
          <Input placeholder="请输入密保答案"></Input>
        </Form.Item>
        <Form.Item
          name="uname"
          rules={[{ required: true, message: "请输入用户名" }]}
        >
          <Input placeholder="请输入用户名"></Input>
        </Form.Item>
        <Form.Item
          name="pwd"
          // rules={[{ required: true, message: "请输入密码" }]}
          // 自定义验证规则
          rules={[
            {
              validator: (_, value) => {
                if (!value) return Promise.reject("请输入密码");
                if (value.length < 2 || value.length > 10) {
                  return Promise.reject(new Error("用户名只能2-10"));
                }
                return Promise.resolve();
              },
              validateTrigger: ["onBlur", "onChange"],
            },
          ]}
        >
          <Input placeholder="请输入密码"></Input>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            登录
          </Button>
        </Form.Item>
      </Form>
    </Container>
  );
}

export default Login;
