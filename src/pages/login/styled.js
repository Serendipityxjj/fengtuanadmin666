import styled from "styled-components";
export const Container = styled.div`
  width: 100%;
  height: 100%;
  background-color: #2d3a4b;
  display: flex;
  justify-content: center;
  align-items: center;
  .ant-form {
    width: 300px;
    h1 {
      color: #fff;
      font-size: 30px;
      text-align: center;
      font-weight: 800;
    }
    button {
      width: 100%;
    }
    .ant-input {
      height: 40px;
    }
    .ant-select .ant-select-selector {
      height: 40px;
      padding-top: 5px;
    }
  }
`;
