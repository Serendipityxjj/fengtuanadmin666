// 导入样式
import { Container } from "./styled";
// 导入组件
import UserEdit from "./components/UserEdit";
import {
  DeleteOutlined,
  FormOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  Card,
  Button,
  Input,
  DatePicker,
  Table,
  Pagination,
  Modal,
  message,
} from "antd";
import { useEffect, useState } from "react";
import { getUsersAction } from "./store/actionCreator";
import { connect } from "react-redux";
import { deleteUsersApi } from "../../api/users";
// 导入API
const { confirm } = Modal;
const { RangePicker } = DatePicker;
const { Search } = Input;
// 分页

// 分页

// 表格数据
function Users(props) {
  const onChange1 = (page, pageSize) => {
    console.log(page, pageSize, 111);
    setParams({
      ...params,
      pagenum: page,
    });
  };
  //****
  // 用户编辑状态
  const [state, setState] = useState(false);
  const [userEditRow, setUserEditRow] = useState({});
  const onSearch = (value) => console.log(value);
  const onChange = (value, dateString) => {
    console.log("Selected Time: ", value);
    console.log("Formatted Selected Time: ", dateString);
  };

  const onOk = (value) => {
    console.log("onOk: ", value);
  };
  // ****
  // 表格数据
  const columns = [
    {
      title: "编号",
      dataIndex: "user_id",
      key: "user_id",
    },
    {
      title: "所属角色",
      dataIndex: "role_name",
      key: "role_name",
    },
    {
      title: "用户名",
      dataIndex: "username",
      key: "username",
    },
    {
      title: "手机号",
      dataIndex: "mobile",
      key: "mobile",
    },
    {
      title: "更新于",
      dataIndex: "create_time",
      key: "create_time",
    },

    {
      title: "操作",
      dataIndex: "address",
      key: "address",
      width: 200,
      fixed: "right",
      render: (text, record, index) => {
        const delFn = (text, record, index) => {
          confirm({
            title: "请确认是否删除",
            icon: <ExclamationCircleOutlined />,
            content: "此操作不可逆",
            async onOk() {
              console.log("OK");
              console.log(text, record, index, "ok");
              let params = { user_id: record.user_id };
              let res = await deleteUsersApi(params);
              if (res.meta.state === 200) {
                message.success(res.meta.msg);
                props.handleInitData(params);
              }
              console.log(res);
            },
            onCancel() {
              console.log("Cancel");
            },
          });
        };
        const editFn = (text, record, index) => {
          console.log(text, record, index, "editFn");
          setState(true);
        };
        return (
          <>
            <Button
              style={{ marginRight: "10px" }}
              onClick={() => {
                console.log(text, record, index);
                delFn(text, record, index);
              }}
            >
              <DeleteOutlined />
            </Button>
            <Button
              onClick={() => {
                editFn(text, record, index);
                setUserEditRow(record);
              }}
            >
              <FormOutlined />
            </Button>
          </>
        );
      },
    },
  ];
  // ****

  // 生命周期
  let [params, setParams] = useState({
    pagenum: 1,
    pageSize: 10,
  });
  useEffect(() => {
    props.handleInitData(params);
    // eslint-disable-next-line
  }, [params.pagenum]);

  // 模态框状态
  return (
    <Container>
      <UserEdit state={state} close={() => setState(false)} row={userEditRow} />
      <Card title="用户列表" extra={<Button type="primary">创建</Button>}>
        <div className="filter">
          {/* 搜索 */}
          <Search
            placeholder="请输入用户名"
            onSearch={onSearch}
            enterButton
            style={{ width: "300px" }}
          />
          &nbsp;&nbsp;
          <RangePicker
            showTime={{ format: "HH:mm" }}
            format="YYYY-MM-DD HH:mm:ss"
            onChange={onChange}
            onOk={onOk}
          />
          {/* 搜索 */}
        </div>
        {/* 表格 */}
        <Table
          rowKey="user_id"
          columns={columns}
          dataSource={props.tableData.list}
          pagination={false}
        />
        {/* 表格 */}
        {/* 分页 */}
        <Pagination
          onChange={onChange1}
          onPage={(page, pageSize) => {
            console.log(page, pageSize, 111);
            setParams({
              ...params,
              pagenum: page,
            });
          }}
          defaultCurrent={1}
          total={parseInt(props.tableData.total)}
          style={{ marginTop: "10px", width: "100%", textAlign: "right" }}
        />
        {/* 分页 */}
      </Card>
    </Container>
  );
}
const mapStateToProps = (state) => {
  console.log("拿到数据", state.toJS());
  return {
    tableData: state.toJS().users.tableData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleInitData: (params) => dispatch(getUsersAction(params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Users);
