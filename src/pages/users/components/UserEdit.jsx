// 导入样式
import { Container } from "./edit.styled";

// 导入组件
import { Button, Form, Select, Input, Modal, message } from "antd";
// 导入API
import { putUsersApi } from "../../../api/users";
function UsersCreate(props) {
  // 路由相关
  const onFinish = async (values) => {
    // 表单登录相关
    let data = { ...props.row, ...values };
    let res = await putUsersApi(data);
    if (res.meta.state === 500) {
      message.error(res.meta.msg);
    } else if (res.meta.state === 200) {
      message.success(res.meta.msg);
    }
  };
  const onFinishFailed = (errorInfo) => {
    console.log("error", errorInfo);
  };
  // 对话框数据
  const handleOk = () => {
    // setIsModalVisible(false);
    props.close();
    console.log("确定");
  };
  const handleCancel = () => {
    // setIsModalVisible(false);
    props.close();
    console.log("取消");
  };
  // 表单回显数据
  console.log(props.row);
  return (
    <Modal
      visible={props.state}
      onOk={handleOk}
      onCancel={handleCancel}
      title="用户编辑"
      destroyOnClose={true}
    >
      <Container>
        {/* 表单区域 */}
        <Form
          size="large"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          initialValues={{
            mobile: props.row.mobile,
            username: props.row.username,
            password: props.row.password,
            question: props.row.passwd_question,
            answer: props.row.passwd_answer,
          }}
        >
          <Form.Item
            label="手机号"
            name="mobile"
            rules={[{ required: true, message: "请输入手机号" }]}
          >
            <Input placeholder="请输入手机号"></Input>
          </Form.Item>
          <Form.Item
            label="账号"
            name="username"
            rules={[{ required: true, message: "请输入账号" }]}
          >
            <Input placeholder="请输入账号"></Input>
          </Form.Item>
          <Form.Item
            label="密码"
            name="password"
            rules={[{ required: true, message: "请输入密码" }]}
          >
            <Input placeholder="请输入密码"></Input>
          </Form.Item>
          <Form.Item
            label="密保问题"
            name="question"
            rules={[{ required: true, message: "请选择密保问题" }]}
          >
            <Select placeholder="请选择密保问题">
              <Select.Option value="你爷爷的名字">你爷爷的名字</Select.Option>
              <Select.Option value="你奶奶的名字">你奶奶的名字</Select.Option>
              <Select.Option value="您其中一位老师的名字">
                您其中一位老师的名字
              </Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="密保答案"
            name="answer"
            rules={[{ required: true, message: "请输入密保答案" }]}
          >
            <Input placeholder="请输入密保答案"></Input>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              修改
            </Button>
            &nbsp; &nbsp;
          </Form.Item>
        </Form>
        {/* 表单区域 */}
      </Container>
    </Modal>
  );
}
export default UsersCreate;
