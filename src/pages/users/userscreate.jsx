// 导入样式
import { Container } from "./create.styled";

// 导入组件
import { Card, Button, Form, Select, Input, message } from "antd";
// 导入API
import { useNavigate } from "react-router-dom";
import { postUsersApi } from "../../api/users";
function UsersCreate() {
  // 路由相关
  const navigator = useNavigate();
  const onFinish = async (values) => {
    // 表单登录相关
    let res = await postUsersApi(values);
    if (res.meta.state === 201) {
      message.success(res.meta.msg);
      navigator("/admin/users");
    }
  };
  const onFinishFailed = (errorInfo) => {
    console.log("error", errorInfo);
  };
  return (
    <Container>
      <Card
        title="用户创建"
        extra={
          <Button
            onClick={() => {
              navigator("/admin/users");
            }}
            type="primary"
          >
            返回
          </Button>
        }
        style={{
          width: "100%",
        }}
      >
        {/* 表单区域 */}
        <Form
          size="large"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="手机号"
            name="mobile"
            rules={[{ required: true, message: "请输入手机号" }]}
          >
            <Input placeholder="请输入手机号"></Input>
          </Form.Item>
          <Form.Item
            label="账号"
            name="username"
            rules={[{ required: true, message: "请输入账号" }]}
          >
            <Input placeholder="请输入账号"></Input>
          </Form.Item>
          <Form.Item
            label="密码"
            name="password"
            rules={[{ required: true, message: "请输入密码" }]}
          >
            <Input placeholder="请输入密码"></Input>
          </Form.Item>
          <Form.Item
            label="密保问题"
            name="question"
            rules={[{ required: true, message: "请选择密保问题" }]}
          >
            <Select placeholder="请选择密保问题">
              <Select.Option value="你爷爷的名字">你爷爷的名字</Select.Option>
              <Select.Option value="你奶奶的名字">你奶奶的名字</Select.Option>
              <Select.Option value="您其中一位老师的名字">
                您其中一位老师的名字
              </Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="密保答案"
            name="answer"
            rules={[{ required: true, message: "请输入密保答案" }]}
          >
            <Input placeholder="请输入密保答案"></Input>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              创建
            </Button>
            &nbsp; &nbsp;
            <Button>重置</Button>
          </Form.Item>
        </Form>
        {/* 表单区域 */}
      </Card>
    </Container>
  );
}
export default UsersCreate;
