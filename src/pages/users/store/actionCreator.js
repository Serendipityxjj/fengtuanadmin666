import { getUsersApi } from "../../../api/users";

export const getUsersAction = (params) => {
  return async (dispatch) => {
    let res = await getUsersApi(params);
    res.data.total = parseInt(res.data.total);
    if (res.meta.state === 200) {
      dispatch({ type: "USERS/SET_TABLEDATA", payload: res.data });
    }
  };
};
