import { fromJS } from "immutable";

const initData = fromJS({
  tableData: {
    list: [],
    total: 0,
  },
});

const reducer = (state = initData, action) => {
  switch (action.type) {
    case "USERS/SET_TABLEDATA":
      return state.setIn(["tableData"], action.payload);

    default:
      break;
  }
  return state;
};
export default reducer;
